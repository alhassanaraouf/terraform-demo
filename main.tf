terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.0.0"
    }
  }
}

provider "aws" {
  # Configuration options
  # profile = "34ml" # uncomment if you use profiles in aws cli and change the profile name
  region  = "eu-central-1"
}
