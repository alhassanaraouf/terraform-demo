# terraform-demo

## Name
WordPress Deployment using Terraform

## Description

Deploying WordPress (Docker Image) to AWS with Terraform
Currently, it does:

1. Make VPC
2. Create 2 public subnets
3. create 2 Security Groups one for ECS container and one for RDS
4. create internet gateway and assigned to our VPC
5. create route table and assigned to our subnets
6. create ECS Cluster
7. Make WordPress Task Definition to deploy WordPress container on fargate
8. make service that Guarantee at least one WordPress Container is working
9. create EFS for persistent volume between running containers
10. make RDS for database (MySQL)
   
## Installation

1. installing Terraform
    > sudo apt-get update && sudo apt-get install -y gnupg software-properties-common curl
    > 
    > curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
    >
    > sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
    > 
    > sudo apt-get update && sudo apt-get install terraform

2. Install AWS CLI
    > curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
    > 
    > unzip awscliv2.zip
    > 
    > sudo ./aws/install

3. Configure AWS CLI
    > aws configure

    and follow the steps it says

4. Installing Infracost (Optional for Cost Estimations)
    > curl -fsSL https://raw.githubusercontent.com/infracost/infracost/master/scripts/install.sh | sh

    then get your API key

    > infracost register

    or (if you registered before)

    > infracost configure get api_key

## Usage
1. clone this repo

2. initialize the project and install AWS provider
    > terraform init

3. see the plan
    > terraform plan

4. apply the plan
    > terraform apply

5. see the cost estimate with Infracost (optional)
   > infracost breakdown --path .
