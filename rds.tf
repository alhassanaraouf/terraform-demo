resource "aws_db_subnet_group" "rds-subnet-group" {
  name       = "mysql-subnet-group"
  subnet_ids = [aws_subnet.pub_subnet.id, aws_subnet.pub2_subnet.id]
}

resource "aws_db_instance" "default" {
  allocated_storage = 10
  engine            = "mysql"
  engine_version    = "5.7"
  instance_class    = "db.t3.micro"
  db_name           = "wordpress"
  username          = "wordpress"
  password          = "wordpress"
  #   parameter_group_name = "default.mysql5.7"
  db_subnet_group_name   = aws_db_subnet_group.rds-subnet-group.name
  vpc_security_group_ids = [aws_security_group.rds_sg.id, aws_security_group.ecs_sg.id]
  skip_final_snapshot    = true
  publicly_accessible    = true
}
