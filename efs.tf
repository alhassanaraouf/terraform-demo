
resource "aws_efs_file_system" "word_data" {

}

resource "aws_efs_access_point" "word_access_point" {
  file_system_id = aws_efs_file_system.word_data.id
}



resource "aws_efs_mount_target" "word_data-subnet1" {
  file_system_id  = aws_efs_file_system.word_data.id
  subnet_id       = aws_subnet.pub_subnet.id
  security_groups = [aws_security_group.ecs_sg.id, aws_security_group.rds_sg.id]
}

resource "aws_efs_mount_target" "word_data-subnet2" {
  file_system_id  = aws_efs_file_system.word_data.id
  subnet_id       = aws_subnet.pub2_subnet.id
  security_groups = [aws_security_group.ecs_sg.id, aws_security_group.rds_sg.id]
}