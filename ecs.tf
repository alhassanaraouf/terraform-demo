resource "aws_ecs_cluster" "wordpress" {
  name = "wordpress"

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

resource "aws_ecs_cluster_capacity_providers" "wordpress_capacity_providers" {
  cluster_name = aws_ecs_cluster.wordpress.name

  capacity_providers = ["FARGATE"]

}





resource "aws_ecs_task_definition" "worker" {
  family       = "wordpress"
  network_mode = "awsvpc"
  cpu          = 256
  memory       = 512
  task_role_arn = aws_iam_role.ecs_task_role_role.arn
  # execution_role_arn = "${aws_iam_role.ecs_task_role_role.arn}"
  container_definitions = jsonencode([
    {
      name      = "wordpress-app"
      image     = "docker.io/library/wordpress:latest"
      essential = true
      portMappings = [
        {
          containerPort = 80
          hostPort      = 80
        }
      ]
      environment = [
        { "name" : "WORDPRESS_DB_HOST", "value" : aws_db_instance.default.address },
        { "name" : "WORDPRESS_DB_NAME", "value" : "wordpress" },
        { "name" : "WORDPRESS_DB_PASSWORD", "value" : "wordpress" },
        { "name" : "WORDPRESS_DB_USER", "value" : "wordpress" }
      ]
      mountPoints = [
        { "containerPath" : "/var/www/html", "sourceVolume" : "word_data" }
      ]
    }
  ])

  volume {
    name = "word_data"

    efs_volume_configuration {
      file_system_id          = aws_efs_file_system.word_data.id
      root_directory          = "/"
      transit_encryption      = "ENABLED"
      transit_encryption_port = 2999
      authorization_config {
        access_point_id = aws_efs_access_point.word_access_point.id
        iam             = "ENABLED"
      }
    }
  }

}





resource "aws_ecs_service" "worker" {
  name = "worker"
  network_configuration {
    subnets          = [aws_subnet.pub_subnet.id, aws_subnet.pub2_subnet.id]
    security_groups  = [aws_security_group.ecs_sg.id, aws_security_group.rds_sg.id]
    assign_public_ip = true
  }
  launch_type     = "FARGATE"
  cluster         = aws_ecs_cluster.wordpress.id
  task_definition = aws_ecs_task_definition.worker.arn
  desired_count   = 1
}


resource "aws_iam_role" "ecs_task_role_role" {
  name = "test-ecs-task-role"
  assume_role_policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Service": "ecs-tasks.amazonaws.com"
            },
            "Action": "sts:AssumeRole",
            "Condition": {}
        }
    ]
  })
  
  tags = {
    Terraform = "true"
  }
}


resource "aws_iam_role_policy" "efsPolicy" {
  name = "test_policy"
  role = aws_iam_role.ecs_task_role_role.id

  policy = data.aws_iam_policy_document.inline_policy.json
}


data "aws_iam_policy_document" "inline_policy" {
  statement {
    actions   = ["elasticfilesystem:ClientMount", "elasticfilesystem:ClientWrite", "elasticfilesystem:ClientRootAccess"]
    resources = [aws_efs_file_system.word_data.arn]
    condition {
      test = "StringEquals"
      variable = "elasticfilesystem:AccessPointArn"
      values = [aws_efs_access_point.word_access_point.id]
    }
  }
}